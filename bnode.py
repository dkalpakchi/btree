__author__ = 'dmitriy'

#
# Defines the class of node of b-tree. Each b-node has the following characteristics:
#  => keys_num - number of keys currently stored in node x
#  =>     keys - the list of keys stored in nondecreasing order
#  =>     leaf - the boolean value, true if node is leaf, false otherwise
#


import bisect


class Bnode(object):
    def __init__(self, keys_num = 0, keys_list = None, root_node = None, leaf = True):
        self.__keys_num = keys_num
        self.__keys = [] if keys_list is None else keys_list
        self.__leaf = leaf
        self.__children = []
        self.__root = root_node

    @property
    def keys(self):
        return self.__keys

    @keys.setter
    def keys(self, keys):
        if isinstance(keys, list):
            self.__keys = keys
        else: raise TypeError("keys property must be a list")

    def add_key(self, new_key):
        bisect.insort(self.__keys, new_key)

    @property
    def keys_num(self):
        return self.__keys_num

    @keys_num.setter
    def keys_num(self, value):
        self.__keys_num = value

    @property
    def leaf(self):
        return self.__leaf

    @leaf.setter
    def leaf(self, value):
        self.__leaf = value

    @property
    def children(self):
        return self.__children

    @children.setter
    def children(self, value):
        self.__children = value

    def has_children(self):
        return len(self.__children) != 0

    @property
    def root(self):
        return self.__root

    @root.setter
    def root(self, value):
        self.__root = value


# b = Bnode()
# print b.keys
# b.keys = [1]
# print b.keys
# b.add_key(20)
# print b.keys
# b.add_key(7)
# print b.keys
# print b.keys[0]
# print b.keys_num
# print b.leaf

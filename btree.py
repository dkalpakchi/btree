__author__ = 'dmitriy'


from bnode import Bnode


class Btree(object):
    def __init__(self, minimum_degree = 2):
        self.__root = Bnode()
        self.__t = minimum_degree if minimum_degree >= 2 else 2
        self.NODE_MIN_KEYS = self.__t - 1
        self.NODE_MAX_KEYS = 2*self.__t - 1

    @property
    def root(self):
        return self.__root

    def split_child(self, x, i, y):
        '''
        split a node y around median key
        :param x: a nonfull internal node
        :param i: an index
        :param y: a node, such that y = x.children[i] is a full child of x
        :return: None
        '''
        z = Bnode(keys_num = self.NODE_MIN_KEYS, leaf = y.leaf, root_node = x)
        z.keys = [y.keys.pop() for j in range(0, self.NODE_MIN_KEYS)]
        z.keys.reverse()

        if not y.leaf:
            z.children = [y.children.pop() for j in range(0, self.__t)]
            z.children.reverse()
        y.keys_num = self.NODE_MIN_KEYS

        x.children.insert(i+1, z)
        x.add_key(y.keys.pop())
        x.keys_num += 1

    def insert_non_full(self, x, key):
        i = x.keys_num - 1
        if x.leaf:
            x.add_key(key)
            x.keys_num += 1
        else:
            while i >= 0 and key < x.keys[i]:
                i -= 1
            i += 1
            if x.children[i].keys_num == self.NODE_MAX_KEYS:
                self.split_child(x, i, x.children[i])
                if key > x.keys[i]:
                    i += 1
            self.insert_non_full(x.children[i], key)

    def insert(self, key):
        root = self.__root
        if root.keys_num == self.NODE_MAX_KEYS:
            s = Bnode(keys_num = 0, leaf = False)
            self.__root = s
            root.root = self.__root
            s.children.insert(0, root)
            self.split_child(s, 0, root)
            self.insert_non_full(s, key)
        else: self.insert_non_full(root, key)

    def search(self, x, key):
        '''
        search for key in btree, starting with the node x
        :param x: the current node of search procedure
        :param key: the key to search
        :return: either the keys of node and index i of desired key or false
        '''
        i = 0
        while i < x.keys_num and key > x.keys[i]:
            i += 1

        if i < x.keys_num and key == x.keys[i]:
            return (x.keys, i)

        return False if x.leaf else self.search(x.children[i], key)

    def delete(self, x, key):
        '''
        you ask to delete the key from subtree rooted with x
        :param x: the root of current subtree
        :param key: the key to delete
        :return:
        '''
        def move_key(k, n1, n2):
            n2.add_key(k)
            n2.keys_num += 1
            n1.keys.remove(k)
            n1.keys_num -= 1

        def move_child(c, n1, n2):
            n2.children.append(n1.children[c])
            n1.children.pop(c)

        def merge_nodes(n1, n2):
            for key in n2.keys:
                n1.add_key(key)
            n1.keys_num += n2.keys_num
            n2.keys_num = 0
            n1.children.extend(n2.children)

        def preceding_sibling(x, index):
            '''
            :param x: the parent node of our current node x.children[index]
            :param index: index of our current node in x.children
            :return: the preceding sibling of x.children[index]
            '''
            return x.children[index - 1] if index > 0 else None

        def succeeding_sibling(x, index):
            '''
            :param x: the parent node of our current node x.children[index]
            :param index: index of our current node in x.children
            :return: the successor sibling of x.children[index]
            '''
            return x.children[index + 1] if index + 1 < len(x.children) else None

        def preceding_child(x, index):
            return preceding_sibling(x, index + 1)

        def succeeding_child(x, index):
            return succeeding_sibling(x, index)

        def find_predecessor_key(x):
            return x.keys[-1]

        def find_successor_key(x):
            return x.keys[0]

        def recurse_to_key(x):
            next = min(map(lambda k: (k[0], abs(key - k[1])), enumerate(x.keys)), key = lambda x: x[1])
            index = next[0] + int(key - x.keys[next[0]] > 0)
            return (next[0], index, x.children[index])

        if key in x.keys and x.leaf:
            x.keys.remove(key)
            x.keys_num -= 1
        elif key not in x.keys:
            ix, iy, y = recurse_to_key(x)
            if y.keys_num == self.NODE_MIN_KEYS:
                predecessor = preceding_sibling(x, iy)
                successor = succeeding_sibling(x, iy)
                if predecessor is not None and predecessor.keys_num > self.NODE_MIN_KEYS:
                    move_key(x.keys[iy], x, y)
                    move_key(predecessor.keys[-1], predecessor, x)
                    if not y.leaf: move_child(-1, predecessor, y)
                elif successor is not None and successor.keys_num > self.NODE_MIN_KEYS:
                    move_key(x.keys[iy], x, y)
                    move_key(successor.keys[0], successor, x)
                    if not y.leaf: move_child(0, successor, y)
                else:
                    drag_parent = x.keys[ix]
                    if predecessor is not None:
                        move_key(drag_parent, x, predecessor)
                        merge_nodes(y, predecessor)
                        x.children.remove(predecessor)
                    elif successor is not None:
                        move_key(drag_parent, x, y)
                        merge_nodes(y, successor)
                        x.children.remove(successor)
            if x.keys_num == 0:
                merge_nodes(x, y)
                x.children.remove(y)
                self.delete(x, key)
            else:
                self.delete(y, key)
        else:
            ik = x.keys.index(key)
            predecessor = preceding_child(x, ik)
            successor = succeeding_child(x, ik)
            if predecessor is not None and predecessor.keys_num > self.NODE_MIN_KEYS:
                replacer = find_predecessor_key(predecessor)
                move_key(replacer, predecessor, x)
                move_key(key, x, successor)
                self.delete(successor, key)
            elif successor is not None and successor.keys_num > self.NODE_MIN_KEYS:
                replacer = find_successor_key(successor)
                move_key(replacer, successor, x)
                move_key(key, x, predecessor)
                self.delete(predecessor, key)
            else:
                merge_nodes(predecessor, successor)
                x.children.remove(successor)
                move_key(key, x, predecessor)
                self.delete(predecessor, key)

    def print_tree(self):
        def print_children(node, level):
            if len(node.children) != 0:
                for c in node.children:
                    print "#" + str(level) + " ->", c.keys
                    print_children(c, level + 1)

        print "--- START ---"
        print "#0 ->", self.__root.keys
        print_children(self.__root, 1)
        print "---  END  ---\n"

# bt = Btree(3)
# bt.insert(10)
# bt.insert(100)
# bt.insert(500)
# bt.insert(1000)
# bt.insert(1500)
# bt.insert(250)
# bt.insert(1300)
# bt.insert(1600)
# bt.insert(1700)
# bt.insert(1800)
# bt.insert(1900)
# bt.insert(2000)
# bt.insert(2001)
# bt.insert(2050)
# bt.insert(2100)
# bt.insert(2500)
# bt.insert(300)
# bt.insert(350)
# bt.insert(400)
# bt.insert(2600)
# bt.insert(2700)
# bt.insert(2900)
# bt.print_tree()
#
# bt.delete(bt.root, 1300)
# bt.print_tree()
# bt.delete(bt.root, 500)
# bt.print_tree()
# bt.delete(bt.root, 400)
# bt.print_tree()

def main():
    def insert():
        key = input("Enter key to insert:")
        bt.insert(key)
        bt.print_tree()

    def delete():
        key = input("Enter key to delete:")
        bt.delete(bt.root, key)
        bt.print_tree()

    def search():
        key = input("Enter key to search for:")
        print bt.search(bt.root, key)

    n = input("Enter the minimum degree of btree to be created (by default it's 2):")
    bt = Btree(n)
    options = {
        'ins' : insert,
        'del' : delete,
        'search' : search,
        'exit' : quit
    }
    while True:
        # try:
        option = raw_input("Choose option please (ins/del/search/exit):")
        options[option.strip()]()
        # except:
        #     print "Smth has gone wrong. Please try again"

if __name__ == '__main__':
    main()
